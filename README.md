## Xiaomi Firmware Packages For Mi MIX (lithium)

#### Downloads: [![DownloadSF](https://img.shields.io/badge/Download-SourceForge-orange.svg)](https://sourceforge.net/projects/xiaomi-firmware-updater/files/Developer) [![DownloadAFH](https://img.shields.io/badge/Download-AndroidFileHost-brightgreen.svg)](https://www.androidfilehost.com/?w=files&flid=241903)

| ID | MIUI Name | Device Name | Codename |
| --- | --- | --- | --- |
| 317 | MIMIX | Xiaomi Mi MIX | lithium |
| 317 | MIMIXGlobal | Xiaomi Mi MIX Global | lithium |

### XDA Main Thread:
[Go here](https://forum.xda-developers.com/android/software-hacking/devices-xiaomi-firmware-updater-t3741446)

#### by [Xiaomi Firmware Updater](https://github.com/XiaomiFirmwareUpdater)
#### Developer: [xiaomi-firmware-updater](https://github.com/xiaomi-firmware-updater)
